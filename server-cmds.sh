#!/usr/bin/env bash

export IMAGE=$1
docker rm -f my-app
docker rm -f mongodb
docker rm -f mongo-express
docker-compose -f docker-compose.yaml up -d